public class HealthInsurance {

	static double basePremium=5000;
	public static void calucatePercentageIncreaseOnAge(PersonDetails p){
		double percentageIncrease=0.0;
			if(p.perAge>18){
				if(p.gender.equalsIgnoreCase("M")){
					percentageIncrease=percentageIncrease+(basePremium*0.12);
				}else{
					percentageIncrease=percentageIncrease+(basePremium*0.1);
				}
			}
		if(p.perAge>25){
			if(p.gender.equalsIgnoreCase("M")){
				percentageIncrease=percentageIncrease+(basePremium*0.12);
			}else{
				percentageIncrease=percentageIncrease+(basePremium*0.1);
			}
		}
		if(p.perAge>30){
			
			if(p.gender.equalsIgnoreCase("M")){
				percentageIncrease=percentageIncrease+(basePremium*0.12);
			}else{
				percentageIncrease=percentageIncrease+(basePremium*0.1);
			}
		}
		if( p.perAge>35){
			if(p.gender.equalsIgnoreCase("M")){
				percentageIncrease=percentageIncrease+(basePremium*0.12);
			}else{
				percentageIncrease=percentageIncrease+(basePremium*0.1);
			}
		}
		if(p.perAge>40){
			if(p.gender.equalsIgnoreCase("M")){
				percentageIncrease=percentageIncrease+(basePremium*0.22);
			}else{
				percentageIncrease=percentageIncrease+(basePremium*0.2);
			}

		}
		basePremium=basePremium+percentageIncrease;
	}
	public static void preExistingConditions(PersonDetails.HabitValues  habits){
		  
	if(habits.alcohol.equalsIgnoreCase("yes"))
		basePremium=basePremium+(basePremium*0.03);
	if(habits.drugs.equalsIgnoreCase("yes"))
		basePremium=basePremium+(basePremium*0.03);
	if(habits.dailyExercise.equalsIgnoreCase("yes"))
		basePremium=basePremium-(basePremium*0.03);
	if(habits.smoking.equalsIgnoreCase("yes"))
		basePremium=basePremium+(basePremium*0.03);
	
	}
	public static void habits(PersonDetails.HealthValues healthConditions){
		  
		if(healthConditions.bp.equalsIgnoreCase("yes"))
			basePremium=basePremium+(basePremium*0.01);
		if(healthConditions.hypertension.equalsIgnoreCase("yes"))
			basePremium=basePremium+(basePremium*0.01);
		if(healthConditions.overWeight.equalsIgnoreCase("yes"))
			basePremium=basePremium+(basePremium*0.01);
		if(healthConditions.sugar.equalsIgnoreCase("yes"))
			basePremium=basePremium+(basePremium*0.01);
		
		}
	public static void main(String[] args) {
		
		PersonDetails p=new PersonDetails("Norman Gomes",34,"M");
		PersonDetails.HabitValues habits=p.new HabitValues("no","yes","yes","no");
		PersonDetails.HealthValues health=p.new HealthValues("no","no","no","yes");
		if(!p.errorFlag) {
		calucatePercentageIncreaseOnAge(p);
		preExistingConditions(habits);
		habits(health);
		System.out.println("Health Insurance Premium for "+p.name+":"+basePremium);
		}
	}

}
