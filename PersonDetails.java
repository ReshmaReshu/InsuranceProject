public class PersonDetails {
   public String name="";
   public int perAge;
   public String gender="";
   public boolean errorFlag;
   public  class HabitValues { 

       public  String smoking=""; 
       public  String alcohol="";
       public  String dailyExercise="";
       public  String drugs="";
       HabitValues(String smoking,String alcohol,String dailyExcercise,String drugs){
            if(!"yes".equalsIgnoreCase(smoking)  && !"no".equalsIgnoreCase(smoking)){
               System.out.println("Invalid input for smoking, Please pass either yes/no ");
               errorFlag=true;
           }else{
              this.smoking=smoking;  
           }
            if(!"yes".equalsIgnoreCase(alcohol)  && !"no".equalsIgnoreCase(alcohol)){
               System.out.println("Invalid input for alcohol, Please pass either yes/no ");
               errorFlag=true;
               
           }else{
                this.alcohol=alcohol;
           }
            if(!"yes".equalsIgnoreCase(dailyExcercise)  && !"no".equalsIgnoreCase(dailyExcercise)){
               System.out.println("Invalid input for dailyExercise, Please pass either yes/no");
               errorFlag=true;
           }else{
              this.dailyExercise=dailyExcercise;  
           }
            if(!"yes".equalsIgnoreCase(drugs)  && !"no".equalsIgnoreCase(drugs)){
               System.out.println("Invalid input for drugs, Please pass either yes/no");
               errorFlag=true;
           }else{
                this.drugs=drugs;
           }
	  
       }

   }
   public  class HealthValues { 

       public  String hypertension=""; 
       public  String bp="" ;
       public  String sugar="";
       public  String overWeight="" ;
       
       HealthValues(String hypertension,String bp,String sugar,String overWeight){
           if(!"yes".equalsIgnoreCase(hypertension)  && !"no".equalsIgnoreCase(hypertension)){
               System.out.println("Invalid input for hypertension, Please pass either yes/no ");
               errorFlag=true;
           }else{
               this.hypertension=hypertension;
           }
            if(!"yes".equalsIgnoreCase(bp)  && !"no".equalsIgnoreCase(bp)){
               System.out.println("Invalid input for blood pressure, Please pass either yes/no ");
               errorFlag=true;
           }else{
                 this.bp=bp;
           }
            if(!"yes".equalsIgnoreCase(sugar)  && !"no".equalsIgnoreCase(sugar)){
               System.out.println("Invalid input for sugar, Please pass either yes/no");
               errorFlag=true;
           }else{
              	   this.sugar=sugar;  
           }
            if(!"yes".equalsIgnoreCase(overWeight)  && !"no".equalsIgnoreCase(overWeight)){
               System.out.println("Invalid input for overWeight, Please pass either yes/no");
               errorFlag=true;
           } else{
               this.overWeight=overWeight;
           }
	  
       }

   }
   
   PersonDetails(String name,int perAge,String gender){
	  
	    if(name==null || "".equals(name)){
               System.out.println("Invalid input for name, Please pass name other than null / empty string");
               errorFlag=true;
           }else{
                this.name=name;
           }
	     if(perAge<=0 || perAge>150 ){
               System.out.println("Invalid input for age, Please pass age in between 1 and 150");
               errorFlag=true; 
           }else{
               
	         this.perAge=perAge;
           }
	   if(!"M".equalsIgnoreCase(gender) && !"F".equalsIgnoreCase(gender)  && !"O".equalsIgnoreCase(gender) ){
               System.out.println("Invalid input for gender, Please pass F/M/O");
               errorFlag=true;
           }else{
                this.gender=gender;
           }
   }

}
